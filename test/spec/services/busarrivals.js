'use strict';

describe('Service: busArrivals', function () {

  // load the service's module
  beforeEach(module('routePlannerApp'));

  // instantiate service
  var busArrivals;
  beforeEach(inject(function (_busArrivals_) {
    busArrivals = _busArrivals_;
  }));

  it('should do something', function () {
    expect(!!busArrivals).toBe(true);
  });

});

'use strict';

describe('Service: busStop', function () {

  // load the service's module
  beforeEach(module('routePlannerApp'));

  // instantiate service
  var busStop;
  beforeEach(inject(function (_busStop_) {
    busStop = _busStop_;
  }));

  it('should do something', function () {
    expect(!!busStop).toBe(true);
  });

});

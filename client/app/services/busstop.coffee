"use strict"
angular.module "routePlannerApp"
.factory "busStop", ($resource, api) ->
  
  stop = {}

  options =
    minlon : ''
    minlat : ''
    maxlon : ''
    maxlat : ''

  busStop = {
    endpoint: ->
        $resource("//transportapi.com/v3/uk/bus/stops/bbox.json?minlon=#{@options.minlon}&minlat=#{@options.minlat}&maxlon=#{@options.maxlon}&maxlat=#{@options.maxlat}&api_key=#{api.api_key}&app_id=#{api.app_id}",
          {},
          jsonpquery: { 
            method: 'JSONP', 
            isArray: true,
            params: {
              
            }
          }
        )
    setOptions: (options) ->
      @options = options
      console.log @options
    getStop : (options) ->
      @endpoint(@setOptions(options))
  }
  
  # Public API here
  busStop
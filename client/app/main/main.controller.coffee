'use strict'

angular.module 'routePlannerApp'
.controller 'MainCtrl', ($scope, busStop, busArrivals) ->

  $scope.currentLocation = '';

  getSomeStops = ->
    positions = minMaxLatLon()

    busStop.getStop(
      minlon : positions.long_min
      minlat : positions.lat_min
      maxlon : positions.long_max
      maxlat : positions.lat_max
    ).jsonpquery(->
      console.log 'success'
    )

  $scope.marker = {
    id: 0
    coords:
        latitude: 51.4915394
        longitude:  -0.0958366
    options:
      draggable: true
    icon: "assets/images/marker.png"
  }

  $scope.map =
    center:
      latitude: 51.520525
      longitude: -0.128378
    zoom: 15
    draggable: true
  
  drawMap = ->
    $scope.map =
      center:
        latitude: $scope.currentLocation.latitude || 51.520525
        longitude: $scope.currentLocation.longitude || -0.128378
      zoom: 15
      draggable: true

  show_map = (position) ->
    $scope.currentLocation =
      latitude : position.coords.latitude
      longitude : position.coords.longitude
    drawMap()
    minMaxLatLon()
    getSomeStops()

  handle_error = (error) ->
    throw "An error : #{error}"

  get_location = () ->
    if navigator.geolocation
      navigator.geolocation.getCurrentPosition(show_map, handle_error)

  minMaxLatLon = () ->
    lat_center = $scope.currentLocation.latitude
    long_center = $scope.currentLocation.longitude
    geoZone5km =
      lat_min : lat_center - 0.045
      lat_max : lat_center + 0.045
      long_min : long_center - (0.045 / Math.cos(lat_center*Math.PI/180))
      long_max : long_center + (0.045 / Math.cos(lat_center*Math.PI/180))

  get_location()

